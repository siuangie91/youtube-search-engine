# YouTube Search Engine #

Project in jQuery and YouTube Data API v3 with FancyBox plugin. From Eduonix's [**Projects In JavaScript & JQuery course**](https://stackskills.com/courses/projects-in-javascript-jquery) on **Stackskills**. 

## Output Search Results ##
![screencapture-file-Users-AngieSiuPC-Desktop-Tutorials-Coding-Misc-_Projects-JS-20Practice-YouTube-20Search-20Engine-index-html-1450065494370.png](https://bitbucket.org/repo/yqx76q/images/463693054-screencapture-file-Users-AngieSiuPC-Desktop-Tutorials-Coding-Misc-_Projects-JS-20Practice-YouTube-20Search-20Engine-index-html-1450065494370.png)

## View Video in FancyBox ##
![Screen.png](https://bitbucket.org/repo/yqx76q/images/1185385914-Screen.png)